extends Node

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var posicion1 = Vector2(120,300)
var posicion2 = Vector2(270,300)
var posicion3 = Vector2(420,300)
var posicion4 = Vector2(570,300)
var posicion5 = Vector2(720,300)
var posicion6 = Vector2(870,300)
onready var Mate = get_node("Mate");
onready var Mate2 = get_node("Mate2");
onready var Mate3 = get_node("Mate3");
onready var Fisica = get_node("Fisica");
onready var Fisica2 = get_node("Fisica2");
onready var Quimica = get_node("Quimica");
var velocidad = 400
var movimiento = Vector2(velocidad,0)

func _ready():
	Mate.set_position(posicion2)
	Mate2.set_position(posicion3)
	Mate3.set_position(posicion1)
	Fisica.set_position(posicion6)
	Fisica2.set_position(posicion4)
	Quimica.set_position(posicion5)
	pass

func _process(delta):
	mover(Mate, posicion2,delta)
	mover(Mate2, posicion4, delta)
	mover(Quimica, posicion1,delta)
	mover(Fisica, posicion3, delta)
	mover(Fisica2,posicion5, delta)
	mover(Mate3, posicion6,delta)
	pass
	
func mover(vector, posicion,delta):
	var distancia = vector.get_position().distance_to(posicion)
	if(distancia > 2):
		if(vector.get_position().angle_to(posicion) < 0 ):
			vector.set_position(vector.get_position()+movimiento *delta)
		elif(vector.get_position().angle_to(posicion) > 0):
			vector.set_position(vector.get_position()-movimiento *delta)
	pass
